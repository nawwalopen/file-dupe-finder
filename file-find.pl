#!/usr/bin/perl -w

=begin comment
This script recursively traverses the directory indicated at $dir and creates sha256
hashes of each file regardless of extension.  The sha256 value is recorded in a hash
data struct as a key with the file path as the key value.  If there is already a key-
value pair, the file with the oldest stat date is maintained as the sha key value. If
both files have the same stat epoch date, then the file with the first ascii sorted
filename is used as the sha key value.  The 'other' filename is placed in an array
for potential deletion.

The script will not delete anything by default - it only identifies duplicates. In
order to delete all encountered duplicates, add the flag '-warshot' to the execution
statement.
=cut

## Begin declarations ###################################################################
use File::Find;
use Digest::SHA qw(sha256);

my %params = map { $_ => 1 } @ARGV;

my %filesha;
my @dupes_to_delete;

############################################
## This is the top level directory searched

my $dir = '/Volumes/media/pictures/US/2003';

############################################


## Begin main ###########################################################################

find(\&findsha, $dir);

my $numsha = keys %filesha;

print "Number of unique sha is $numsha\n";

my $num_to_delete = scalar @dupes_to_delete;

print "Num duplicate files is $num_to_delete \n";

foreach ( @dupes_to_delete ) {
  print "$_";
  if ( exists($params{"-warshot"}) ) {
    unlink ($_) or next "Can't unlink $_: $!\n";
    print "   unlinked";
  }
  print "\n";
}


## endgame ##############################################################################

## Begin subroutines ####################################################################
sub findsha
{
    if (-d) {
      return;
    }
    
    if (-f) {

      my $fullpath=$File::Find::name;
      
      my $sha = Digest::SHA->new(256);
      $sha->addfile($fullpath);
      my $hash = $sha->hexdigest();

      if (exists($filesha{$hash}))
      {
        print "------------------------------------\n"; 
        my $epoch_timestamp1 = (stat($filesha{$hash}))[9];
        my $timestamp1       = localtime($epoch_timestamp1);
        my $file1name         = $filesha{$hash};
        print "file1: -> $epoch_timestamp1 $timestamp1    $file1name\n";

        my $epoch_timestamp2 = (stat($fullpath))[9];
        my $timestamp2       = localtime($epoch_timestamp2);
        my $file2name        = $fullpath;
        print "file2: -> $epoch_timestamp2 $timestamp2    $file2name\n";
        print "------------------------------------\n"; 
        
        if ( $epoch_timestamp2 == $epoch_timestamp1 ) {
          if ( $filesha{$hash} lt $fullpath ) {
            print "timestamps are equal file1 name lt file2 name - would delete file2 $file2name\n";
            push @dupes_to_delete, $file2name;
            $filesha{$hash} = $file1name;
          } else {
            print "timestamps are equal file2 name lt file1 name - would delete file1 $file1name\n";
            push @dupes_to_delete, $file1name;
            $filesha{$hash} = $file2name;
          }
          return;
        }

        if ( $epoch_timestamp2 < $epoch_timestamp1 ) { 
          print "file2 time $timestamp2 is older ";
          print "would delete file1 $timestamp1 $file1name\n";
          push @dupes_to_delete, $file1name;
          $filesha{$hash} = $file2name;
          return;
        } else { 
          print "file1 time $timestamp1 is older ";
          print "would delete file2 $timestamp2 $file2name\n";
          push @dupes_to_delete, $file2name;
          $filesha{$hash} = $file1name;
          return;
        } #end if ( $epoch_timestamp2 < $epoch_timestamp1 )

      } else {
        $filesha{$hash} = $fullpath;
      }

    } #end if (-f)

} #end sub findsha

######################