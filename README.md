# file-dupe-finder

This Perl script will recursively traverse a directory and report files that have identical SHA256 hashes.  I use it to look through my photo directories and find duplicate images.